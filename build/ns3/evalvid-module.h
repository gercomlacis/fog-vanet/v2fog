
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_EVALVID
    

// Module headers:
#include "evalvid-client-server-helper.h"
#include "evalvid-client.h"
#include "evalvid-server.h"
#endif
