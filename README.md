
V2Fog Simulation
================================

## Table of Contents:

1) [Script "Timeline"](#Simulation script)
2) [Decision Making](#Topsis)
3) [Running the ns-3 simulation](# Running the ns-3 simulation)

## Simulation script

Line 45 to 143 --> Is defined the trace .txt for analysis and some structs for the simulation.

Line 144 to 306 --> Is defined the simulation parameters and variables.

Line 308 to 1960 --> Is defined the functions for the simulation and the basic for the network to properly work in NS3. In this sense the functions names are self explanatory. 

Line 1961 to 2164 --> The "main" function of the simulation, calling the functions needed and schedulling the decision making scheme.

Note: More info and comments are provided inside the simulation.cc

## Topsis

The topsis decision is called inside the simulation.cc by a function (topsis), which in turn calls the decision.py to apply the topsis and return the best vehicle to be the fog.

The topsis defined values inside decision.py are:

influence= [0, 0, 1]  if higher is better=1 else=0

weights = [0.5, 0.35, 0.15] the values correspond respetively to distance, usage and Throughput.

Note: The topsis needs the Numpy and Topsis library installed to work correctly. Both are available at
https://pypi.org/project/topsis-jamesfallon/
https://numpy.org/install/


## Running the ns-3 simulation

Note:  How to install and much more substantial information about ns-3 can be found at
https://www.nsnam.org

To configure and remove warnings error
CXXFLAGS="-Wall -g -O0" ./waf configure

```shell
./waf --run simulation.cc
```

The program should generate some text trace file to collect data.


