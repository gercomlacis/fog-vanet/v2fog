from topsis import topsis
import numpy as np
import sys

#Take x and y positions of ue device and return the id for the enb

pos_x=float(sys.argv[1])
pos_y=float(sys.argv[2])

# if higher is better =1 else =0 (Distance,usage,Throughput)
influence= [0, 0, 1]
pesos = [0.5, 0.35, 0.15]

def main():
    inputs=[]
    with open("uav-status.txt") as f:
        lines = [[int(i.split()[1]),float(i.split()[2]), float(i.split()[3]),float(i.split()[4]),float(i.split()[5])] for i in f.readlines()]
        if (len(lines) == 0):
            return
        lines = np.array(lines[-30:])
        for i in lines:
            inputs.append([np.linalg.norm(np.array((i[1],i[2]))-np.array((pos_x,pos_y))),i[3],i[4]])
        try:
            decision = topsis(inputs, pesos, influence)
            #print (topsis(inputs, pesos, influence))
            decision.calc()
            best = decision.optimum_choice
            #print  (decision.optimum_choice)
            if isinstance(lines[best][0], (int, float)):
                print(int(lines[best][0]))
            else:
                print(0)
        except Exception:
            pass
if __name__ == "__main__":
    main()
